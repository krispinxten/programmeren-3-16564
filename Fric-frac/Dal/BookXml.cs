﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.IO;

namespace Fricfrac.Dal
{
    class BookXml : IBook
    {
        public Models.Book Book { get; set; }
    public string Message { get; set; }
    private string connectionString = @"Data/Book";
    public string ConnectionString
    {
        get { return connectionString + ".xml"; }
        set { connectionString = value; }
    }
        public BookXml()
        {
                
        }
    public BookXml(Models.Book book)
    {
        Book = book;
    }
    public BookXml(string connectionString)
    {
        ConnectionString = connectionString;
    }
    public bool Create()
    {
        try
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Models.Book[]));
            TextWriter writer = new StreamWriter(ConnectionString);
                Models.Book[] books = Book.List.ToArray();
            serializer.Serialize(writer, books);
            writer.Close();
            Message = $"Bestand {ConnectionString} is met succes geserializeerd.";
            return true;
        }
        catch (Exception e)
        {
            Message = $"Bestand {ConnectionString} is niet geserialiseerd.\nFoutmelding {e.Message}.";
            return false;
        }

    }
    public bool ReadAll()
    {
        try
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Models.Book[]));
            StreamReader file = new System.IO.StreamReader(ConnectionString);
                Models.Book[] books = (Models.Book[])serializer.Deserialize(file);
            file.Close();
            foreach (Models.Book item in books)
            {
                if (item.Author != null) item.Author = item.Author.Replace("\n", "").Replace("\r", "");
                if (item.City != null) item.City = item.City.Replace("\n", "").Replace("\r", "");
                if (item.Comments != null) item.Comments = item.Comments.Replace("\n", "").Replace("\r", "");
                if (item.Edition != null) item.Edition = item.Edition.Replace("\n", "").Replace("\r", "");
                if (item.Publisher != null) item.Publisher = item.Publisher.Replace("\n", "").Replace("\r", "");
                if (item.Title != null) item.Title = item.Title.Replace("\n", "").Replace("\r", "");
                if (item.Translator != null) item.Translator = item.Translator.Replace("\n", "").Replace("\r", "");
                if (item.Year != null) item.Year = item.Year.Replace("\n", "").Replace("\r", "");
            }
            Book.List = new List<Models.Book>(books);
            Message = $"Bestand {ConnectionString} is met succes gedeserialiseerd.";
            return true;
        }
        catch (Exception e)
        {
            Message = $"Het bestand {ConnectionString} s niet gedeserialiseerd.\nFoutmelding {e.Message}.";
            return false;
        }

    }
}
}
