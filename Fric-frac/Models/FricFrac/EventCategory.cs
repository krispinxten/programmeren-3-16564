﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Fricfrac.Models.Fricfrac
{
    public partial class EventCategory
    {
        public EventCategory()
        {
        }

        [Required]
        [StringLength(120)]
        [FromForm(Name = "EventCategory-Name")]
        public string Name { get; set; }
        [Column(TypeName = "int(11)")]
        [FromForm(Name = "EventCategory-Id")]
        public int Id { get; set; }
    }
}