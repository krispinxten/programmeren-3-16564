﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;

namespace AdoDotNet
{
    class Learning
    {
        /// <summary>
        /// Het testen van een connectie met de server connectionstring is hardcoded
        /// </summary>
        public static void TestMySqlConnector()
        {
            MySqlConnection connection = new MySql.Data.MySqlClient.MySqlConnection();
            string ConnectionString = "server= 164.132.193.60; user id=Student4;password=Student_DOSADCZ6;port=3306;database=Student4;SslMode=none;";
            connection.ConnectionString = ConnectionString;
            Console.WriteLine("Connectie gemaakt.");
            using (connection)
            {
                MySqlCommand command = new MySqlCommand();
                string sqlStatement = "select Name, Id from EventCategory;";
                command.Connection = connection;
                command.CommandText = sqlStatement;
                connection.Open();
                MySqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Console.WriteLine("{0}\t{1}", reader["Name"],
                            reader["Id"]);
                    }
                }
                else
                {
                    Console.WriteLine("No rows found.");
                }
                reader.Close();
            }
            Console.ReadKey();
        }

        /// <summary>
        /// Het testen van EventCategory dal
        /// </summary>
        public static void FricFracDalTest()
        {

            // testen Readall
            //----------------
            Console.WriteLine("Fric-frac DAL test EventCategory");
            FricFrac.Dal.EventCategory dal = new FricFrac.Dal.EventCategory();
            List<FricFrac.Bll.EventCategory> list = dal.ReadAll();
            Console.WriteLine($"{dal.RowCount} {dal.Message}");
            foreach (FricFrac.Bll.EventCategory item in list)
                Console.WriteLine("{0} {1}", item.Id, item.Name);
            // testen ReadOne
            //----------------
            dal.ReadOne(4);
            Console.WriteLine($"{dal.RowCount} {dal.Message}");
            dal.ReadOne(200);
            Console.WriteLine($"{dal.RowCount} {dal.Message}");
            // testen toevoegen
            //----------------
            // we proberen deze gevonden categorie weer toe te voegen
            FricFrac.Bll.EventCategory bll = new FricFrac.Bll.EventCategory();
            bll = dal.ReadOne(4);
            Console.WriteLine($" {dal.RowCount} {dal.Message}");
            // we lezen een bestaande categorie in

            // we proberen deze gevonden categorie weer toe te voegen
            dal.Create(bll);
            Console.WriteLine($" {dal.RowCount} {dal.Message}");
            // we wijzigen de naam van de categorie
            bll.Name = "Hackathon Programmeren";
            // En proberen die toe te voegen
            dal.Create(bll);
            Console.WriteLine($" {dal.RowCount} {dal.Message}");
            // testen update
            //----------------
            // we zoeken de naam van de categorie
            bll.Name = "Hackathon Programmeren";
            dal.ReadOne(bll.Id);
            // we wijzigen de naam van de categorie met name = "Hackathon Programmeren"
            bll.Name = "Hackathon Programmeren Deel 2";
            // En proberen die te updaten
            dal.Update(bll);
            Console.WriteLine($" {dal.RowCount} rij(en) gewijzigd, {dal.Message}");

            // testen delete
            //----------------
            // nu gaan we de Hackaton categorie deleten (bij mij Id = 22)
            // je moet de Id's nakijken, zeker als je experimenteert met de code
            dal.Delete(22);
            Console.WriteLine($" {dal.RowCount} rij(en) gedeleted, {dal.Message}");
            // zet de naam van categorie met Id = 4 terug op Convention
            // We hebben dit daarnet gewijzigd in Hackathon Programmeren Deel 2
            bll.Id = 4;
            bll.Name = "Convention";
            dal.Update(bll);
            // En toon het nog eens
            list = dal.ReadAll();
            Console.WriteLine($" {dal.RowCount} {dal.Message}");
            foreach (FricFrac.Bll.EventCategory item in list)
                Console.WriteLine(" {0} {1}", item.Id, item.Name);
        }
        public static void FricFracDalTest2()
        {

            // testen Readall
            //----------------
            Console.WriteLine("Fric-frac DAL test EventTopic");
            FricFrac.Dal.EventTopic dal = new FricFrac.Dal.EventTopic();
            List<FricFrac.Bll.EventTopic> list = dal.ReadAll();
            Console.WriteLine($"{dal.RowCount} {dal.Message}");
            foreach (FricFrac.Bll.EventTopic item in list)
                Console.WriteLine("{0} {1}", item.Id, item.Name);
            // testen ReadOne
            //----------------
            dal.ReadOne(4);
            Console.WriteLine($"{dal.RowCount} {dal.Message}");
            dal.ReadOne(200);
            Console.WriteLine($"{dal.RowCount} {dal.Message}");
            // testen toevoegen
            //----------------
            // we proberen deze gevonden topic weer toe te voegen
            FricFrac.Bll.EventTopic bll = new FricFrac.Bll.EventTopic();
            bll = dal.ReadOne(4);
            Console.WriteLine($" {dal.RowCount} {dal.Message}");
            // we lezen een bestaande topic in

            // we proberen deze gevonden topic weer toe te voegen
            dal.Create(bll);
            Console.WriteLine($" {dal.RowCount} {dal.Message}");
            // we wijzigen de naam van de topic
            bll.Name = "Hackathon Programmeren";
            // En proberen die toe te voegen
            dal.Create(bll);
            Console.WriteLine($" {dal.RowCount} {dal.Message}");
            // testen update
            //----------------
            // we zoeken de naam van de topic
            bll.Name = "Hackathon Programmeren";
            dal.ReadOne(bll.Id);
            // we wijzigen de naam van de topic met name = "Hackathon Programmeren"
            bll.Name = "Hackathon Programmeren Deel 2";
            // En proberen die te updaten
            dal.Update(bll);
            Console.WriteLine($" {dal.RowCount} rij(en) gewijzigd, {dal.Message}");

            // testen delete
            //----------------
            // nu gaan we de Hackaton categorie deleten (bij mij Id = 21)
            // je moet de Id's nakijken, zeker als je experimenteert met de code
            dal.Delete(21);
            Console.WriteLine($" {dal.RowCount} rij(en) gedeleted, {dal.Message}");
            // zet de naam van categorie met Id = 4 terug op Convention
            // We hebben dit daarnet gewijzigd in Hackathon Programmeren Deel 2
            bll.Id = 4;
            bll.Name = "Convention";
            dal.Update(bll);
            // En toon het nog eens
            list = dal.ReadAll();
            Console.WriteLine($" {dal.RowCount} {dal.Message}");
            foreach (FricFrac.Bll.EventTopic item in list)
                Console.WriteLine(" {0} {1}", item.Id, item.Name);
        }
        public static void FricFracDalTest3()
        {
            // testen ReadName
            //----------------
            // we zoeken de naam van de topic
            
            Console.WriteLine("Fric-frac DAL test Name /LikeName / LikeXName");
            FricFrac.Dal.EventTopic dal = new FricFrac.Dal.EventTopic();
            Console.WriteLine("testen ReadName");
            dal.ReadByName("Auto, Boat & Air");
            Console.WriteLine($"{dal.RowCount} {dal.Message}");
            // testen ReadLikeName
            //----------------
            // we zoeken de eerste stuk naam van de topic

            Console.WriteLine("testen ReadLikeName");
            List<FricFrac.Bll.EventTopic> list = dal.ReadLikeName("Con");
            Console.WriteLine($"{dal.RowCount} {dal.Message}");
            foreach (FricFrac.Bll.EventTopic item in list)
                Console.WriteLine("{0} {1}", item.Id, item.Name);
            // testen ReadLikeXName
            //----------------
            // we zoeken een stuk uit de naam van de topic
            Console.WriteLine("testen ReadLikeXName");
            List<FricFrac.Bll.EventTopic> listx = dal.ReadLikeXName("Ho");
            Console.WriteLine($"{dal.RowCount} {dal.Message}");
            foreach (FricFrac.Bll.EventTopic item in listx)
                Console.WriteLine("{0} {1}", item.Id, item.Name);
        }
        public static void FricFracDalTest4()
        {
            // testen ReadName
            //----------------
            // we zoeken de naam van de topic

            Console.WriteLine("Fric-frac DAL test Name /LikeName / LikeXName");
            FricFrac.Dal.EventCategory dal = new FricFrac.Dal.EventCategory();
            Console.WriteLine("testen ReadName");
            dal.ReadByName("Trip or Retreat");
            Console.WriteLine($"{dal.RowCount} {dal.Message}");
            // testen ReadLikeName
            //----------------
            // we zoeken de eerste stuk naam van de topic

            Console.WriteLine("testen ReadLikeName");
            List<FricFrac.Bll.EventCategory> list = dal.ReadLikeName("Con");
            Console.WriteLine($"{dal.RowCount} {dal.Message}");
            foreach (FricFrac.Bll.EventCategory item in list)
                Console.WriteLine("{0} {1}", item.Id, item.Name);
            // testen ReadLikeXName
            //----------------
            // we zoeken een stuk uit de naam van de topic
            Console.WriteLine("testen ReadLikeXName");
            List<FricFrac.Bll.EventCategory> listx = dal.ReadLikeXName("To");
            Console.WriteLine($"{dal.RowCount} {dal.Message}");
            foreach (FricFrac.Bll.EventCategory item in listx)
                Console.WriteLine("{0} {1}", item.Id, item.Name);
        }
    }
        
}

