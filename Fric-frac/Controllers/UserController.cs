﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Fricfrac.Models.Fricfrac;

namespace Fricfrac.Controllers
{
    public class UserController : Controller
    {
        private readonly Student13Context dbContext;
        // voeg constructor toe om geïnjecteerde DBContext 
        // te kunnen binnenkrijgen in deze klasse
        public UserController(Student13Context dbContext)
        {
            this.dbContext = dbContext;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            ViewBag.Title = "Fric-frac User Index";
            return View(dbContext.User.ToList());
        }

        public IActionResult InsertingOne()
        {
            ViewBag.Title = "Fric-frac User Inserting One";
            ViewBag.Persoon = dbContext.Person.ToList();
            ViewBag.Rol = dbContext.Role.ToList();
            return View();
        }

       

        public IActionResult UpdatingOne(int? id)
        {
            ViewBag.Title = "Fric-frac User Updating One";
            if (id == null)
            {
                return NotFound();
            }
            var user = dbContext.User.SingleOrDefault(m => m.Id == id);
            if (user == null)
            {
                return NotFound();
            }
            user.Person = dbContext.Person.SingleOrDefault(m => m.Id == user.PersonId);
            ViewBag.Persoon = dbContext.Person.ToList();
            user.Role = dbContext.Role.SingleOrDefault(m => m.Id == user.RoleId);
            ViewBag.Rol = dbContext.Role.ToList();
            return View(user);
        }

        public IActionResult Cancel()
        {
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult InsertOne(User user)
        {
            ViewBag.Message = "Insert een User in de database";
            dbContext.User.Add(user);
            dbContext.SaveChanges();
            return View("Index", dbContext.User);
        }
        [HttpGet]
        public IActionResult ReadingOne(int? id)
        {
            ViewBag.Message = "Lees een User in de database";
            if (id == null)
            {
                return NotFound();
            }

            var user = dbContext.User.SingleOrDefault(m => m.Id == id);
            if (user == null)
            {
                return NotFound();
            }
            user.Person = dbContext.Person.SingleOrDefault(m => m.Id == user.PersonId);
            user.Role = dbContext.Role.SingleOrDefault(m => m.Id == user.RoleId);
            return View(user);
        }

        [HttpPost]
        public IActionResult UpdateOne(User user)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    dbContext.Update(user);
                    dbContext.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!dbContext.User.Any(e => e.Id == user.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return View("Index", dbContext.User);
        }

        // GET: Supplier/Delete/5
        public IActionResult DeleteOne(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var user = dbContext.User.SingleOrDefault(m => m.Id == id);
            if (user == null)
            {
                return NotFound();
            }
            dbContext.User.Remove(user);
            dbContext.SaveChanges();
            // keer terug naar de index pagina
            return RedirectToAction("Index");
        }
    }
}
