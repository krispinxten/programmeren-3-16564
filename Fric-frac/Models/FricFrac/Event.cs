﻿using Fricfrac.Models.Fricfrac;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Fricfrac.Models.Fricfrac
{
    public partial class Event
    {
        public Event()
        {  
        }

        [Required]
        [StringLength(120)]
        [FromForm(Name = "Event-Name")]
        public string Name { get; set; }
        [Required]
        [StringLength(120)]
        [FromForm(Name = "Event-Location")]
        public string Location { get; set; }
        [Column(TypeName = "datetime")]
        [FromForm(Name = "Event-StartTime")]
        public DateTime? Starts { get; set; }
        [Column(TypeName = "datetime")]
        [FromForm(Name = "Event-EndTime")]
        public DateTime? Ends { get; set; }
        [Required]
        [StringLength(255)]
        [FromForm(Name = "Event-Image")]
        public string Image { get; set; }
        [Required]
        [StringLength(1024)]
        [FromForm(Name = "Event-Desc")]
        public string Description { get; set; }
        [Required]
        [StringLength(120)]
        [FromForm(Name = "Event-OrganiserName")]
        public string OrganiserName { get; set; }
        [Required]
        [StringLength(120)]
        [FromForm(Name = "Event-OrganiserDesc")]
        public string OrganiserDescription { get; set; }
        [Column(TypeName = "int(11)")]
        [FromForm(Name = "Event-EventCategoryId")]
        public int? EventCategoryId { get; set; }
        [Column(TypeName = "int(11)")]
        [FromForm(Name = "Event-EventTopicId")]
        public int? EventTopicId { get; set; }
        [Column(TypeName = "int(11)")]
        [FromForm(Name = "Event-Id")]
        public int Id { get; set; }

        [ForeignKey("EventCategoryId")]
        public EventCategory EventCategory { get; set; }
        [ForeignKey("EventTopicId")]
        public EventTopic EventTopic { get; set; }
    }
}
