﻿using Fricfrac.Models.Fricfrac;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Fricfrac.Models.Fricfrac
{
    public partial class User
    {
        [Required]
        [StringLength(50)]
        [FromForm(Name = "User-Name")]
        public string Name { get; set; }
        [StringLength(255)]
        [FromForm(Name = "User-Salt")]
        public string Salt { get; set; }
        [StringLength(255)]
        [FromForm(Name = "User-HashedPassword")]
        public string HashedPassword { get; set; }
        [Column(TypeName = "int(11)")]
        [FromForm(Name = "User-PersonId")]
        public int? PersonId { get; set; }
        [Column(TypeName = "int(11)")]
        [FromForm(Name = "User-RoleId")]
        public int? RoleId { get; set; }
        [Column(TypeName = "int(11)")]
        [FromForm(Name = "User-Id")]
        public int Id { get; set; }

        [ForeignKey("PersonId")]
        public Person Person { get; set; }
        [ForeignKey("RoleId")]
        public Role Role { get; set; }
    }
}