﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Fricfrac.Models.Fricfrac;

namespace Fricfrac.Controllers
{
    public class EventTopicController : Controller
    {
        private readonly Student13Context dbContext;
        // voeg constructor toe om geïnjecteerde DBContext 
        // te kunnen binnenkrijgen in deze klasse
        public EventTopicController(Student13Context dbContext)
        {
            this.dbContext = dbContext;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            ViewBag.Title = "Fric-frac EventTopic Index";
            return View(dbContext.EventTopic.ToList());
        }
        public IActionResult InsertingOne()
        {
            ViewBag.Title = "Fric-frac EventTopic Inserting One";
            return View(dbContext.EventTopic.ToList());
        }

        [HttpGet]
        public IActionResult UpdatingOne(int? id)
        {
            ViewBag.Title = "Fric-frac EventTopic Updating One";
            if (id == null)
            {
                return NotFound();
            }

            var EventTopic = dbContext.EventTopic.SingleOrDefault(m => m.Id == id);
            if (EventTopic == null)
            {
                return NotFound();
            }
            return View(EventTopic);
        }

        public IActionResult Cancel()
        {
            return RedirectToAction("Index");
        }


        [HttpPost]
        // rechtstreeks ophalen uit de payload
        public IActionResult InsertOneFromRequest()
        {
            Models.Fricfrac.EventTopic EventTopic = new Models.Fricfrac.EventTopic();
            EventTopic.Name = Request.Form["EventTopic-Name"];

            ViewBag.Message = "Insert een EventTopic in de database";
            dbContext.EventTopic.Add(EventTopic);
            dbContext.SaveChanges();
            return View("Index", dbContext.EventTopic);
        }

        [HttpPost]
        public IActionResult InsertOne(Models.Fricfrac.EventTopic EventTopic)
        {
            ViewBag.Message = "Insert een EventTopic in de database";
            dbContext.EventTopic.Add(EventTopic);
            dbContext.SaveChanges();
            return View("Index", dbContext.EventTopic);
        }

        [HttpGet]
        public IActionResult ReadingOne(int? id)
        {
            ViewBag.Message = "Insert een EventTopic in de database";
            if (id == null)
            {
                return NotFound();
            }

            var EventTopic = dbContext.EventTopic.SingleOrDefault(m => m.Id == id);
            if (EventTopic == null)
            {
                return NotFound();
            }
            return View(EventTopic);
        }

        [HttpPost]
        public IActionResult UpdateOne(Models.Fricfrac.EventTopic EventTopic)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    dbContext.Update(EventTopic);
                    dbContext.SaveChanges();
                    return View("ReadingOne", EventTopic);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!dbContext.EventTopic.Any(e => e.Id == EventTopic.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return View("Index", EventTopic);
        }
        // GET: Supplier/Delete/5
        public IActionResult DeleteOne(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var EventTopic = dbContext.EventTopic.SingleOrDefault(m => m.Id == id);
            if (EventTopic == null)
            {
                return NotFound();
            }
            dbContext.EventTopic.Remove(EventTopic);
            dbContext.SaveChanges();
            // keer terug naar de index pagina
            return RedirectToAction("Index");
        }
    }
}
