﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wiskunde.Meetkunde
{
    static class VormKleur
    {
        private static ConsoleColor kleur;
        public static ConsoleColor Kleur
        {
            get
            {
                kleur =Console.ForegroundColor;
                return kleur;
            }
            set
            {
                kleur = value;
            }

        }
    }
}
