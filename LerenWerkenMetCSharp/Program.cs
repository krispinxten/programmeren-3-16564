﻿using System;

namespace Wiskunde.Meetkunde
{
    class Program
    {
        public static string vorm ;
        public static Vormen myvorm;
        static void Main(string[] args)
        {

            Console.WriteLine("vormen tekenen!");
            Console.WriteLine("     Type 'R' voor een rechthoek");
            Console.WriteLine("     Type 'D' voor een driehoek");
            Console.WriteLine("     Type 'K' voor een Kleur te kiezen");
            Console.WriteLine("     Type 'S' voor een symbool te kiezen");
            Console.WriteLine("     Type 'T' voor de vorm te Teken");
            Console.WriteLine("     Druk 'Esc' om te stoppen");
            
            do
                Runtime(); while (Console.ReadKey(true).Key != ConsoleKey.Escape);

        }

        private static void Runtime()
        {
            string keuze = ReadString("Maak een keuze: ");

            string ReadString(string message)

            {
                Console.Write(message);
                return Console.ReadLine();
            }

            switch (keuze)
            {
                case "D":
                case "d":
                    vorm = "Driehoek";
                    break;

                case "R":
                case "r":
                    vorm = "Rechthoek";
                    break;

                case "K":
                case "k":
                    string kleur = ReadString("Geef een Kleur: ");
                    break;

                case "S":
                case "s":
                    string teken = ReadString("Geef een Symbool in: ");
                    break;


                case "T":
                case "t":
                    if (vorm == "Driehoek")
                    {
                        myvorm = new Driehoek();
                    }
                    else
                    {
                        myvorm = new Rechthoek();
                    }
                    myvorm.Teken();
                    break;

                default:
                    break;

            }

            Console.ForegroundColor = ConsoleColor.White;
        }

    }
}
