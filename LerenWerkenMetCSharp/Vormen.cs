﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wiskunde.Meetkunde
{
    public abstract class Vormen
    {
        private ConsoleColor kleur;
        public ConsoleColor Kleur
        {
            get { return kleur; }
            set
            {
                kleur = value;
                Console.ForegroundColor = kleur;
            }
        }
        private char karakter;
        public char Karakter
        {
            get { return karakter; }
            set
            {
                karakter = value;
            }
        }
       
        protected Vormen()
        {
            this.karakter = VormTeken.Karakter;
            this.kleur = VormKleur.Kleur;
        }


        public virtual void Teken()
        {
          
        }
    }
}
