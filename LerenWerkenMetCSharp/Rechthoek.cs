﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wiskunde.Meetkunde
{
    class Rechthoek : Vormen
    {
        public Rechthoek()
           : base()
        {
        }
        public override void Teken()

        {
            int aantalRijen = 5;
            for (int i = 1; i <= aantalRijen; i++)
            { 
                for (int j = 1; j <= aantalRijen; j++)
                {
                    Console.Write(base.Karakter);
                }
                Console.WriteLine();
            }

            Console.WriteLine();
            
        }
    }
}
