﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Fricfrac.Controllers
{
    public class BookController : Controller
    {
        private readonly Dal.IBook dalService;
        public IActionResult Books()
        {
            Models.Book book = new Models.Book();
            dalService.Book = book;
            dalService.ReadAll();
            return View(book);
        }


        public BookController(Dal.IBook dalService)
        {
            this.dalService = dalService;
        }
    }
}