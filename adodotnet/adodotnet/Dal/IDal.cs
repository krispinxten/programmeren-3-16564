﻿using System.Collections.Generic;

namespace FricFrac.Dal
{
    public interface IDal<T>
    {
        string Message { get; }
        int RowCount { get; }
        List<T> ReadAll();
        T ReadOne(int id);
        int Create(T bll);
        int Update(T bll);
        int Delete(int id);
        T ReadByName(string name);
        List<T> ReadLikeName(string name);
        List<T> ReadLikeXName(string name);

    }
}
