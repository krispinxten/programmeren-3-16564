﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wiskunde.Meetkunde
{
    class Driehoek : Vormen
    {

        public Driehoek()
            : base()
        {
        }
        public override void Teken()

        {

            int aantalRijen = 5;

                for (int rijNummer = 0;
                rijNummer <= aantalRijen; 
                rijNummer++)
                {
                    for (int j = 1; j <= aantalRijen - rijNummer; j++)
                    {
                        Console.Write(" ");
                    }
                    for (int j = 1; j <= 2 * rijNummer - 1; j++)
                    {
                        Console.Write(base.Karakter.ToString());
                    }

                    Console.WriteLine();
                }
                Console.WriteLine();
            
        }
    }
}