﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;



namespace Fricfrac.Controllers
{
    public class AdminController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            // wordt gebruikt in het head->title element 
            // de Master Page
            ViewBag.Title = "Fric-frac Admin";
            return View();
        }
    }
}
