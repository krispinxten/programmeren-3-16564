﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Fricfrac.Models.Fricfrac;

namespace Fricfrac.Controllers
{
    public class EventController : Controller
    {
        private readonly Student13Context dbContext;
        // voeg constructor toe om geïnjecteerde DBContext 
        // te kunnen binnenkrijgen in deze klasse
        public EventController(Student13Context dbContext)
        {
            this.dbContext = dbContext;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            ViewBag.Title = "Fric-frac Event Index";
            return View(dbContext.Event.ToList());
        }

        public IActionResult InsertingOne()
        {
            ViewBag.Title = "Fric-frac Event Inserting One";
            ViewBag.EventCategory = dbContext.EventCategory.ToList();
            ViewBag.EventTopic = dbContext.EventTopic.ToList();
            return View();
        }

        //public IActionResult ReadingOne()
        //{
        //    ViewBag.Title = "Fric-frac Event Reading One";
        //    return View();
        //}

        public IActionResult UpdatingOne(int? id)
        {
            ViewBag.Title = "Fric-frac Event Updating One";
            if (id == null)
            {
                return NotFound();
            }
            var Event = dbContext.Event.SingleOrDefault(m => m.Id == id);
            if (Event == null)
            {
                return NotFound();
            }
            Event.EventCategory = dbContext.EventCategory.SingleOrDefault(m => m.Id == Event.EventCategoryId);
            ViewBag.EventCategory = dbContext.EventCategory.ToList();
            Event.EventTopic = dbContext.EventTopic.SingleOrDefault(m => m.Id == Event.EventTopicId);
            ViewBag.EventTopic = dbContext.EventTopic.ToList();
            return View(Event);
        }

        public IActionResult Cancel()
        {
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult InsertOne(Event Event)
        {
            ViewBag.Message = "Insert een Event in de database";
            dbContext.Event.Add(Event);
            dbContext.SaveChanges();
            return View("Index", dbContext.Event);
        }
        [HttpGet]
        public IActionResult ReadingOne(int? id)
        {
            ViewBag.Message = "Lees een Event in de database";
            if (id == null)
            {
                return NotFound();
            }

            var Event = dbContext.Event.SingleOrDefault(m => m.Id == id);
            if (Event == null)
            {
                return NotFound();
            }
            Event.EventCategory = dbContext.EventCategory.SingleOrDefault(m => m.Id == Event.EventCategoryId);
            Event.EventTopic = dbContext.EventTopic.SingleOrDefault(m => m.Id == Event.EventTopicId);
            return View(Event);
        }

        [HttpPost]
        public IActionResult UpdateOne(Event Event)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    dbContext.Update(Event);
                    dbContext.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!dbContext.Event.Any(e => e.Id == Event.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return View("Index", dbContext.Event);
        }

        // GET: Supplier/Delete/5
        public IActionResult DeleteOne(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var Event = dbContext.Event.SingleOrDefault(m => m.Id == id);
            if (Event == null)
            {
                return NotFound();
            }
            dbContext.Event.Remove(Event);
            dbContext.SaveChanges();
            // keer terug naar de index pagina
            return RedirectToAction("Index");
        }
    }
}
