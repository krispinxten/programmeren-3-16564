﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Fricfrac.Models.Fricfrac;

namespace Fricfrac.Controllers
{
    public class EventCategoryController : Controller
    {
        private readonly Student13Context dbContext;
        // voeg constructor toe om geïnjecteerde DBContext 
        // te kunnen binnenkrijgen in deze klasse
        public EventCategoryController(Student13Context dbContext)
        {
            this.dbContext = dbContext;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            ViewBag.Title = "Fric-frac EventCategory Index";
            return View(dbContext.EventCategory.ToList());
        }
        public IActionResult InsertingOne()
        {
            ViewBag.Title = "Fric-frac EventCategory Inserting One";
            return View(dbContext.EventCategory.ToList());
        }

        [HttpGet]
        public IActionResult UpdatingOne(int? id)
        {
            ViewBag.Title = "Fric-frac EventCategory Updating One";
            if (id == null)
            {
                return NotFound();
            }

            var EventCategory = dbContext.EventCategory.SingleOrDefault(m => m.Id == id);
            if (EventCategory == null)
            {
                return NotFound();
            }
            return View(EventCategory);
        }

        public IActionResult Cancel()
        {
            return RedirectToAction("Index");
        }


        [HttpPost]
        // rechtstreeks ophalen uit de payload
        public IActionResult InsertOneFromRequest()
        {
            Models.Fricfrac.EventCategory EventCategory = new Models.Fricfrac.EventCategory();
            EventCategory.Name = Request.Form["EventCategory-Name"];

            ViewBag.Message = "Insert een EventCategory in de database";
            dbContext.EventCategory.Add(EventCategory);
            dbContext.SaveChanges();
            return View("Index", dbContext.EventCategory);
        }

        [HttpPost]
        public IActionResult InsertOne(Models.Fricfrac.EventCategory EventCategory)
        {
            ViewBag.Message = "Insert een Role in de database";
            dbContext.EventCategory.Add(EventCategory);
            dbContext.SaveChanges();
            return View("Index", dbContext.EventCategory);
        }

        [HttpGet]
        public IActionResult ReadingOne(int? id)
        {
            ViewBag.Message = "Insert een EventCategory in de database";
            if (id == null)
            {
                return NotFound();
            }

            var EventCategory = dbContext.EventCategory.SingleOrDefault(m => m.Id == id);
            if (EventCategory == null)
            {
                return NotFound();
            }
            return View(EventCategory);
        }

        [HttpPost]
        public IActionResult UpdateOne(Models.Fricfrac.EventCategory EventCategory)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    dbContext.Update(EventCategory);
                    dbContext.SaveChanges();
                    return View("ReadingOne", EventCategory);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!dbContext.EventCategory.Any(e => e.Id == EventCategory.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return View("Index", EventCategory);
        }
        // GET: Supplier/Delete/5
        public IActionResult DeleteOne(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var EventCategory = dbContext.EventCategory.SingleOrDefault(m => m.Id == id);
            if (EventCategory == null)
            {
                return NotFound();
            }
            dbContext.EventCategory.Remove(EventCategory);
            dbContext.SaveChanges();
            // keer terug naar de index pagina
            return RedirectToAction("Index");
        }
    }
}
