﻿using Fricfrac.Models.Fricfrac;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Fricfrac.Models.Fricfrac
{
    public partial class Person
    {
        public Person()
        {
        }

        [Required]
        [StringLength(50)]
        [FromForm(Name = "Person-FirstName")]
        public string FirstName { get; set; }
        [Required]
        [StringLength(120)]
        [FromForm(Name = "Person-LastName")]
        public string LastName { get; set; }
        [StringLength(255)]
        [FromForm(Name = "Person-Email")]
        public string Email { get; set; }
        [StringLength(255)]
        [FromForm(Name = "Person-Address1")]
        public string Address1 { get; set; }
        [StringLength(255)]
        [FromForm(Name = "Person-Address2")]
        public string Address2 { get; set; }
        [StringLength(20)]
        [FromForm(Name = "Person-PostalCode")]
        public string PostalCode { get; set; }
        [StringLength(80)]
        [FromForm(Name = "Person-City")]
        public string City { get; set; }
        [Required]
        [Column(TypeName = "int(11)")]
        [FromForm(Name = "Person-CountryId")]
        public int? CountryId { get; set; }
        [StringLength(25)]
        [FromForm(Name = "Person-Phone1")]
        public string Phone1 { get; set; }
        [Column(TypeName = "datetime")]
        [FromForm(Name = "Person-Birthday")]
        public DateTime? Birthday
        {
            get; set;
        }
        [Column(TypeName = "int(11)")]
        [FromForm(Name = "Person-Rating")]
        public int? Rating { get; set; }
        [FromForm(Name = "Person-Id")]
        [Column(TypeName = "int(11)")]
        public int Id { get; set; }

        [ForeignKey("CountryId")]
        public Country Country { get; set; }
    }
}