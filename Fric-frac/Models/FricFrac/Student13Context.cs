﻿using System;
using Fricfrac.Models.Fricfrac;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Fricfrac.Models.Fricfrac
{
    public partial class Student13Context : DbContext
    {
        public Student13Context()
        {
        }

        // een constructor argument dat een DbContextOption aanvaardt:
        public Student13Context(DbContextOptions<Student13Context> options)
            : base(options)
        {
        }

        public virtual DbSet<Country> Country { get; set; }
        public virtual DbSet<Event> Event { get; set; }
        public virtual DbSet<EventCategory> EventCategory { get; set; }
        public virtual DbSet<EventTopic> EventTopic { get; set; }
        public virtual DbSet<Person> Person { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<User> User { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
            }
        }
    }
}
