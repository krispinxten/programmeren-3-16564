﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wiskunde.Meetkunde
{
    static class VormTeken
    {
        private static char karakter;

        public static char Karakter
        {
            get
            {
                if (karakter == null || karakter == '\0')
                {
                    karakter = char.Parse("-");
                }
                return karakter;
            }
            set { karakter = value; }
        }
    }
}
