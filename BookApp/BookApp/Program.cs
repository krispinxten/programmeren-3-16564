using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace BookApp
{
    class Program
    {
        static void Main(string[] args)
        {
              // Bll.Book book = new Bll.Book();
              // Dal.BookXml bookXml = new Dal.BookXml(book);
              // bookXml.Book = book;
              // bookXml.ReadAll();
              // Console.WriteLine(bookXml.Message);
              //  Dal.BookCsv bookCsv = new Dal.BookCsv(book);
              // bookCsv.Separator = '|';
              //  bookCsv.Create();
              // Dal.BookJson bookJson = new Dal.BookJson(book);
              //  bookJson.Create();
              //  bookXml.Create();
              //  TryOutCsv();
              //  TryOutXml();
              //  TryOutJson();
              //  Console.ReadLine();
            ServiceCollection serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);
            ServiceProvider serviceProvider = serviceCollection.BuildServiceProvider();
            serviceProvider.GetService<App>().Run();
            Console.ReadLine();
        }

        static void ConfigureServices(IServiceCollection serviceCollection) {
            // config path
            var configuration = new ConfigurationBuilder()
               .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile("appsettings.json", false)
               .Build();
            serviceCollection.AddOptions();
            serviceCollection.Configure<AppSettings>(configuration.GetSection("Configuration"));
            //services
            serviceCollection.AddSingleton<Dal.IBook>(p => new Dal.BookJson(new Bll.Book()));
            serviceCollection.AddTransient<App>();
        }

        static void TryOutCsv()
        {
            Console.WriteLine("De Book App CSV");
            Bll.Book book = new Bll.Book();
            Dal.BookCsv bookCsv = new Dal.BookCsv(book);
            bookCsv.Separator = '|';
            bookCsv.Book = book;
            bookCsv.ReadAll();
            Console.WriteLine(bookCsv.Message);
            View.BookConsole view = new View.BookConsole(book);
            view.List();
            bookCsv.ConnectionString = "Data/BookSemicolon";
            bookCsv.Create(';');
            Console.WriteLine(bookCsv.Message);
        }
        static void TryOutXml()
        {
            Console.WriteLine("De Book App XML");
            Bll.Book book = new Bll.Book();
            Dal.BookXml bookXml = new Dal.BookXml(book);
            bookXml.Book = book;
            bookXml.ReadAll();
            Console.WriteLine(bookXml.Message);
            View.BookConsole view = new View.BookConsole(book);
            view.List();
            bookXml.ConnectionString = "Data/Book2";
            bookXml.Create();
            Console.WriteLine(bookXml.Message);
        }

        static void TryOutJson()
        {
            Console.WriteLine("De Book App Json");
            Bll.Book book = new Bll.Book();
            Dal.BookJson bookJson = new Dal.BookJson(book);
            bookJson.Book = book;
            bookJson.ReadAll();
            Console.WriteLine(bookJson.Message);
            View.BookConsole view = new View.BookConsole(book);
            view.List();
            bookJson.ConnectionString = "Data/Book";
            bookJson.Create();
            Console.WriteLine(bookJson.Message);
        }
    }
}
