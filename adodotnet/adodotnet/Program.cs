﻿using System;

namespace AdoDotNet
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Leren werken met ADO.NET in .NET Core!");


            //Learning.TestMySqlConnector();
            //Learning.FricFracDalTest();
            Learning.FricFracDalTest2();
            Learning.FricFracDalTest3();
            Learning.FricFracDalTest4();
            Console.ReadKey();
        }
    }
}
