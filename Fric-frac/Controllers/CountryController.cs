﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Fricfrac.Models.Fricfrac;
using Microsoft.EntityFrameworkCore;


namespace Fricfrac.Controllers
{
    public class CountryController : Controller
    {
        private readonly Student13Context dbContext;
        // voeg constructor toe om geïnjecteerde DBContext 
        // te kunnen binnenkrijgen in deze klasse
        public CountryController(Student13Context dbContext)
        {
            this.dbContext = dbContext;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            ViewBag.Title = "Fric-frac Country Index";
            return View(dbContext.Country.ToList());
        }

        public IActionResult InsertingOne()
        {
            ViewBag.Title = "Fric-frac Country Inserting One";
            return View(dbContext.Country.ToList());
        }

        [HttpGet]
        public IActionResult UpdatingOne(int? id)
        {
            ViewBag.Title = "Fric-frac Country Updating One";
            if (id == null)
            {
                return NotFound();
            }

            var country = dbContext.Country.SingleOrDefault(m => m.Id == id);
            if (country == null)
            {
                return NotFound();
            }
            return View(country);
        }

        public IActionResult Cancel()
        {
            return RedirectToAction("Index");
        }


        [HttpPost]
        // rechtstreeks ophalen uit de payload
        public IActionResult InsertOneFromRequest()
        {
            Models.Fricfrac.Country Country = new Models.Fricfrac.Country();
            Country.Name = Request.Form["Country-Name"];

            ViewBag.Message = "Insert een land in de database";
            dbContext.Country.Add(Country);
            dbContext.SaveChanges();
            return View("Index", dbContext.Country);
        }

        [HttpPost]
        public IActionResult InsertOne(Models.Fricfrac.Country Country)
        {
            ViewBag.Message = "Insert een land in de database";
            dbContext.Country.Add(Country);
            dbContext.SaveChanges();
            return View("Index", dbContext.Country);
        }

        [HttpGet]
        public IActionResult ReadingOne(int? id)
        {
            ViewBag.Message = "Insert een land in de database";
            if (id == null)
            {
                return NotFound();
            }

            var country = dbContext.Country.SingleOrDefault(m => m.Id == id);
            if (country == null)
            {
                return NotFound();
            }
            return View(country);
        }

        [HttpPost]
        public IActionResult UpdateOne(Models.Fricfrac.Country country)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    dbContext.Update(country);
                    dbContext.SaveChanges();
                    return View("ReadingOne", country);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!dbContext.Country.Any(e => e.Id == country.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return View("Index", country);
        }
        // GET: Supplier/Delete/5
        public IActionResult DeleteOne(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var country = dbContext.Country.SingleOrDefault(m => m.Id == id);
            if (country == null)
            {
                return NotFound();
            }
            dbContext.Country.Remove(country);
            dbContext.SaveChanges();
            // keer terug naar de index pagina
            return RedirectToAction("Index");
        }
    }
}
